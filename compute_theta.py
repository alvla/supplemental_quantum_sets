from ncpol2sdpa import *
import numpy as np
import csv


def drehung(index, gleichung ,b, eps, level, file): #bei systematischen veraenderungen
	n_vertices = 8 #number of vertices of our graph

	"Set edges" #represents the edges of the graph
	if gleichung == 1:
		"CHSH"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #CHSH
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4),(2,6),(3,7)} #CHSH
		test_set = "CHSH"
	elif gleichung == 2:
		"Q_33,33"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(0,4),(2,6)} #Q_33,33
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(3,7)} #Q_33,33
		test_set = "Q_33,33"
	elif gleichung == 3:
		"Q_44,1111"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,1111
		edges_B = {(0,1),(2,3),(4,5),(6,7)} #Q_44,1111
		test_set = "Q_44,1111"
	elif gleichung == 4:
		"Q_44,43"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,43
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4),(3,7)} #Q_44,43
		test_set = "Q_44,43"
	elif gleichung == 5:
		"Q_44,33_1"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,33_1
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(3,7)} #Q_44,33_1
		test_set = "Q_44,33_1"
	elif gleichung == 6:
		"Q_44,311"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,311
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5)} #Q_44,311
		test_set = "Q_44,311"
	elif gleichung == 7:
		"Q_44,411"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,411
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4)} #Q_44,411
		test_set = "Q_44,411"
	elif gleichung == 8:
		"Q43,311"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(2,6),(3,7)} #Q43,311
		edges_B = {(0,1),(2,3),(4,5),(6,7),(0,4)} #Q43,311
		test_set = "Q43,311"
	elif gleichung == 9:
		"Q_411,33"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(2,6)} #Q_411,33
		edges_B = {(0,1),(2,3),(4,5),(6,7),(0,4),(3,7)} #Q_411,33
		test_set = "Q_411,33"
	elif gleichung == 10:
		"Q_43,43_1"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(2,6),(3,7)} #Q_43,43_1
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4),(3,7)} #Q_43,43_1
		test_set = "Q_43,43_1"
	elif gleichung == 11:
		"Q_43,411"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(0,4),(2,6),(3,7)} #Q_43,411
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4)} #Q_43,411
		test_set = "Q_43,411"
	elif gleichung == 12:
		"Q_43,33_1"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(2,6),(3,7)} #Q_43,33_1
		edges_B = {(0,1),(2,3),(4,5),(6,7),(0,4),(3,7)} #Q_43,33_1
		test_set = "Q_43,33_1"
	elif gleichung == 13:
		"Q_43,33_2"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(2,6),(3,7)} #Q_43,33_2
		edges_B = {(0,1),(2,3),(4,5),(6,7),(0,4),(2,6)} #Q_43,33_2
		test_set = "Q_43,33_2"
	elif gleichung == 14:
		"Q_44,33_2"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(1,5),(0,4),(2,6),(3,7)} #Q_44,33_2
		edges_B = {(0,1),(2,3),(4,5),(6,7),(0,4),(3,7)} #Q_44,33_2
		test_set = "Q_44,33_2"
	else: #15
		"Q_43,43_2"
		edges_A = {(1,2),(3,4),(5,6),(0,7),(0,4),(2,6),(3,7)} #Q_43,43_2
		edges_B = {(0,1),(2,3),(4,5),(6,7),(1,5),(0,4),(3,7)} #Q_43,43_2
		test_set = "Q_43,43_2"

	"Adjacency matrices"
	adj_matrix_A = np.zeros((n_vertices,n_vertices))
	adj_matrix_B = np.zeros((n_vertices,n_vertices))
	for i in range(n_vertices):
		for j in range(n_vertices):
			if (i,j) in edges_A:
				adj_matrix_A[i,j] = 1
			if (i,j) in edges_B:
				adj_matrix_B[i,j] = 1

	"Set operators"
	A = generate_operators('A', n_vertices, hermitian=True)
	B = generate_operators('B', n_vertices, hermitian=True)

	"Set objective"
	obj = -sum([b[i]*A[i]*B[i] for i in range(n_vertices)]) #sum of weighted components of the behaviour

	"Substitutions"
	subs = {A[i]**2:A[i] for i in range(n_vertices)}  #conditions of projectors
	subs.update({B[i]**2:B[i] for i in range(n_vertices)})  #conditions of projectors
	(subs.update({B[j]*A[i]:A[i]*B[j] for i in range(n_vertices) for j in
		    range(n_vertices)}))  #symmetry
	(subs.update({A[i]*A[j]:0 for i in range(n_vertices) for j in
		    range(n_vertices) if adj_matrix_A[i,j] == 1}))  #orthogonality relation
	(subs.update({B[i]*B[j]:0 for i in range(n_vertices) for j in
		    range(n_vertices) if adj_matrix_B[i,j] == 1}))  #orthogonality relation

	"Extra monomials"
	extra = ([A[i]*B[j] for i in range(n_vertices) for j in range(n_vertices)])

	"Set problem"
	sdpRelaxation = SdpRelaxation(A+B, verbose=1);
	sdpRelaxation.get_relaxation(level, objective=obj,
		                          substitutions=subs, extramonomials=extra);

	"Solve"
	sdpRelaxation.solve(solver = 'mosek')

	#varianz berechnen
	c = 0
	for i in range(0,8):
		c+=(1/8-b[i])**2
	var = np.sqrt(c)

	#1-norm 
	einsnorm= sum(b)
	zweinorm = np.linalg.norm(b)


	"Daten in csv schreiben"
	with open(file, mode='a') as csvfile:
	    csvfile_writer = csv.writer(csvfile, delimiter=',')
	    csvfile_writer.writerow([index, "1+AB", gleichung,einsnorm, zweinorm, var, -sdpRelaxation.primal, -sdpRelaxation.dual, sdpRelaxation.status, eps, b])

b = [1/8-eps/8,1/8-eps/8,1/8+eps*3/40,1/8+eps*3/40,1/8+eps*3/40,1/8+eps*3/40,1/8+eps*3/40,1/8-eps/8] #weight vector, here: omega_5(-0,-1,-7)
drehung(0, 1, b, eps, 1, "data_test.csv")
